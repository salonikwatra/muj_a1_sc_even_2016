package in.forsk.listactivitywithcursoradapter_sqlite;

import android.app.ListActivity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import in.forsk.listactivitywithcursoradapter_sqlite.DbHelper.FacultyDbHelper;
import in.forsk.listactivitywithcursoradapter_sqlite.adapter.CustomCursorAdapter;
import in.forsk.listactivitywithcursoradapter_sqlite.wrapper.FacultyWrapper;

public class MainActivity extends ListActivity {
    private final static String TAG = MainActivity.class.getSimpleName();
    Context context;
    AQuery aq;

    // Data model (Array List holding objects of FacultyWrapper )
    ArrayList<FacultyWrapper> mFacultyDataList;

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        context = this;

        aq = new AQuery(context);

        // Creating reference

        // Local File Parsing

        aq.ajax("http://demo5392516.mockable.io/faculty_json", JSONArray.class, new AjaxCallback<JSONArray>() {
            @Override
            public void callback(String url, JSONArray object, AjaxStatus status) {
                super.callback(url, object, status);

                Log.d(TAG,object.toString());

                mFacultyDataList = pasreLocalFacultyFile(object.toString());

                FacultyDbHelper.deleteRecords(context);

                for (FacultyWrapper obj : mFacultyDataList) {
                    FacultyDbHelper.insertRecords(context, obj);
                }

                setAdapter();
            }
        });


        //limitation for this implementation
        //1. We can't use aquery for async image loading
        //2. We can't control our view property through code
        //3. Customization is not possible

//		ListAdapter adapter = new SimpleCursorAdapter(this,
//				R.layout.row_faculty_profile_list, FacultyDbHelper.retriveRecords(context), new
// String[] { DBAdapter.FACULTY_COLUMN_FIRSTNAME, DBAdapter.FACULTY_COLUMN_EMAIL,
//						DBAdapter.FACULTY_COLUMN_PHONE }, new int[] { R.id.nameTv, R.id
// .departmentTv, R.id.reserch_areaTv });


        setAdapter();

        // getListView() this method will return list view object, all remain
        // same
        getListView().setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                // TODO Auto-generated method stub
                int itemPosition = position;

                // ListView Clicked item value
                // String itemValue = (String) lv.getItemAtPosition(position);

                // Show Alert
                Toast.makeText(getApplicationContext(), "Position :" + itemPosition, Toast
                        .LENGTH_LONG).show();
            }
        });
    }

    public void setAdapter() {
        //Solution -  we have to use a cusotm cursor adapter (extend), similar we created the
		// custom base adapter
        CustomCursorAdapter adapter = new CustomCursorAdapter(context, FacultyDbHelper
				.retriveRecords(context), 0);

        // Setting adapter to the list view, at this point list view use adapter
        // class methods to fill its view start the recycling process.
        setListAdapter(adapter);
    }

    private String getStringFromRaw(Context context, int resourceId) throws IOException {
        // Reading File from resource folder
        Resources r = context.getResources();
        InputStream is = r.openRawResource(resourceId);
        String statesText = convertStreamToString(is);
        is.close();

        Log.d(TAG, statesText);

        return statesText;
    }

    private String convertStreamToString(InputStream is) throws IOException {
        // Converting input stream into string
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int i = is.read();
        while (i != -1) {
            baos.write(i);
            i = is.read();
        }
        return baos.toString();
    }

    public ArrayList<FacultyWrapper> pasreLocalFacultyFile(String json_string) {

        ArrayList<FacultyWrapper> mFacultyDataList = new ArrayList<FacultyWrapper>();
        try {
            // Converting multipal json data (String) into Json array
            JSONArray facultyArray = new JSONArray(json_string);
            Log.d(TAG, facultyArray.toString());
            // Iterating json array into json objects
            for (int i = 0; facultyArray.length() > i; i++) {

                // Extracting json object from particular index of array
                JSONObject facultyJsonObject = facultyArray.getJSONObject(i);

                // Design patterns
                FacultyWrapper facultyObject = new FacultyWrapper(facultyJsonObject);

                printObject(facultyObject);

                mFacultyDataList.add(facultyObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mFacultyDataList;
    }

    public void printObject(FacultyWrapper obj) {
        // Operator Overloading
        Log.d(TAG, "First Name : " + obj.getFirst_name());
        Log.d(TAG, "Last Name : " + obj.getLast_name());
        Log.d(TAG, "Photo : " + obj.getPhoto());
        Log.d(TAG, "Department : " + obj.getDepartment());
        Log.d(TAG, "reserch_area : " + obj.getReserch_area());
        Log.d(TAG, "Phone : " + obj.getPhone());
        Log.d(TAG, "Email : " + obj.getEmail());

        for (String s : obj.getInterest_areas()) {
            Log.d(TAG, "Interest Area : " + s);
        }
    }

}